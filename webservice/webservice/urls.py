"""webservice URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, register_converter
from api.views import HandleRegisterRequest, module_list, all_professors_average, rate_professor,professor_module_average
from rest_framework.authtoken import views
from . import converts

register_converter(converts.FloatUrlParameterConverter, 'float')


urlpatterns = [
    path('admin/', admin.site.urls),
    path ('api/register' , HandleRegisterRequest ),
    path ('api/list_all_modules',module_list),
    path ('api/average',all_professors_average),
    path ('api/rate/<str:prof_code>/<str:module_code>/<int:year>/<int:semester>/<float:rating>',rate_professor),
    path ('api/average/<str:prof_code>/<str:module_code>',professor_module_average)
    ]

urlpatterns += [
    path('api/api-token-auth/', views.obtain_auth_token)
]