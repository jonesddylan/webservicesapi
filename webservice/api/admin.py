from django.contrib import admin
from .models import Professor,Rating,Module

# Register your models here.
# admin.site.register(Student)
admin.site.register(Professor)
admin.site.register(Rating)
admin.site.register(Module)