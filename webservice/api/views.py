from django.shortcuts import render
# from http.client import HTTPResponse
from django.http import HttpResponse,JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from api.models import Module,Professor,Rating
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from api.serializers import ModuleSerializer,RegisterSerializer
from rest_framework import generics
from rest_framework.authtoken.models import Token
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view
from rest_framework.decorators import authentication_classes,permission_classes
from django.db.models import Avg
import decimal
import json


# registers user
@csrf_exempt
def HandleRegisterRequest(request):

    body = json.loads(request.body)
    serializer_class = RegisterSerializer(data = body)
    if serializer_class.is_valid():
        serializer_class.save()
        return JsonResponse({'message':'User created successfully'})
    else:
        return JsonResponse({'status':'false','message':"Error invalid credentials"}, status=400)
    
# Lists  all modules
@api_view(['GET'])
@authentication_classes([TokenAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def module_list(request):
    if request.method == 'GET':
        qs = Module.objects.prefetch_related('professors')
        listed= ModuleSerializer(qs,many=True).data
        return JsonResponse(listed,safe=False)

# Lists all professors averages across all modules
@api_view(['GET'])
@authentication_classes([TokenAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def all_professors_average(request):
    professors = Professor.objects.all()
    avglist = []
    for prof in professors:
        thisdict = {
            "professor": prof.code_username,
            "average": round(prof.rating_set.all().aggregate(Avg('rating'))['rating__avg']) }
        avglist.append(thisdict)
    return JsonResponse(avglist, safe=False)

# view avg for prof in certain module 
@api_view(['GET'])
@authentication_classes([TokenAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def professor_module_average(request,prof_code,module_code):
    prof = Professor.objects.get(code = prof_code)
    module  = Module.objects.get(code = module_code)
    prof_ratings = Rating.objects.filter(module=module,professor = prof)
    prof_ratings = (prof_ratings.aggregate(Avg('rating'))['rating__avg'])
    if not prof_ratings:
        return JsonResponse({'status':'false','message':"Error invalid module and/or professor"}, status=400)
    else:
        avgRating = {
            'professor' : prof.code_username,
            'module' : module.code_title,
            'rating' : round(prof_ratings)
        }
        return JsonResponse(avgRating)

# Create Rating
@api_view(['POST'])
@authentication_classes([TokenAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def rate_professor(request,prof_code,module_code,year,semester,rating):
    student = request.user
    module  = Module.objects.get(code = module_code,year=year,semester=semester)
    prof = Professor.objects.get(code = prof_code)
    exists = Rating.objects.get(professor = prof,module = module,student=student)
    # 
    if((not module) or (not prof)):
        return JsonResponse({'status':'false','message':'Invalid Module or professor'}, status=400)
    # Checks if rating already exists
    if not exists:
        saveRating = Rating(professor = prof,module = module,student=student,rating=rating)
    else:
        saveRating = Rating(pk=exists.pk,professor = prof,module = module,student=student,rating=rating)
    # Checks all fields are valid
    try:
        print(saveRating.clean_fields())
        saveRating.save()
        return JsonResponse({"message":"Rating successful"})
    except ValidationError:
        return JsonResponse({'status':'false','message':"Error invalid rating, must be from 1.0 to 5.0"}, status=400)