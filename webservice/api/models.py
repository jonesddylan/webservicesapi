import email
from operator import mod
from tkinter import CASCADE
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User

User._meta.get_field('email')._unique = True
User._meta.get_field('email').blank = False
User._meta.get_field('email').null = False

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class Professor(models.Model):
    username = models.CharField(max_length=30)
    code = models.CharField(max_length=3)
    @property
    def code_username(self):
        return("("+self.code+") "+self.username)

    def __str__(self):
        return self.username


class Module(models.Model):
    title = models.CharField(max_length=100)
    year = models.IntegerField()
    semester = models.IntegerField(
        [MinValueValidator(1), MaxValueValidator(3)])
    code = models.CharField(max_length=3)
    students = models.ManyToManyField(settings.AUTH_USER_MODEL)
    professors = models.ManyToManyField(Professor, related_name="modules")
    ratings = models.ManyToManyField(Professor, through='Rating')
    @property
    def code_title(self):
        return("("+self.code+") "+self.title)
    def __str__(self):
        return self.title


class Rating(models.Model):
    professor = models.ForeignKey(Professor, on_delete=models.CASCADE)
    module = models.ForeignKey(Module, on_delete=models.CASCADE)
    student = models.ForeignKey('auth.User', related_name='ratings', on_delete=models.CASCADE)
    rating = models.FloatField(
        validators=[MinValueValidator(1.0), MaxValueValidator(5.0)])

    def __str__(self):
        return (str(self.student)+" Rating "+str(self.professor))
