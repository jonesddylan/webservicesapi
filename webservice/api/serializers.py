# from pyrsistent import field
from rest_framework import serializers
from django.contrib.auth.models import User
from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password
from api.models import Module,Professor,Rating

class ModuleSerializer(serializers.ModelSerializer):
    # codes = CodeSerializer(read_only=True,many=True)
    professors = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field = 'code_username'
    )
    class Meta:
        model = Module
        fields = ['code','title','year','semester','professors']

class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
            required=True,
            validators=[UniqueValidator(queryset=User.objects.all())]
            )
    password = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ('username', 'password','email')

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)
